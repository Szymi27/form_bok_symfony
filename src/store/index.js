import { createStore } from "vuex";
import getters from "@/store/getters";
import actions from "@/store/actions";
import mutations from "@/store/mutations";
import createPersistedState from "vuex-persistedstate";

export default createStore({
  plugins: [createPersistedState({ storage: window.sessionStorage })],
  state: {
    reports: [],
    newReports: [],
    units: [],
    actionStatus: [],
  },
  getters,
  actions,
  mutations,
});
