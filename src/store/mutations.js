export default {
  loadReports(state, payload) {
    state.reports = payload;
  },
  loadNewReports(state, payload) {
    state.newReports = payload;
  },
  setUnits(state, payload) {
    state.units = payload;
  },
  setActions(state, payload) {
    state.actionStatus = payload;
  },

  setFirstReader(state, payload) {
    let report = state.reports.find(
      (report) => report["id"].toString() === payload.id.toString()
    );
    report["readStatus"] = "odczytane";
    report["firstRead"] = payload.firstReader;
    report["firstReadDate"] = payload.firstDate;
  },
  setStatus(state, payload) {
    let report = state.reports.find(
      (report) => report["id"] === payload.reportId
    );
    report["actionStatus"] = payload.statusId;
  },
  addComment(state, payload) {
    let report = state.reports.find((report) => report["id"] === payload.id);
    report["comments"].push({
      user: payload.user,
      comment: payload.comment,
    });
  },
};
