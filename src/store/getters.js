export default {
  reports(state) {
    return state.reports;
  },
  newReports(state) {
    return state.newReports;
  },
  units(state) {
    return state.units;
  },
  actionStatus(state) {
    return state.actionStatus;
  },
};
