export default {
  async loadReports(context) {
    const response = await fetch(`http://form/form/reports`);
    const responseData = await response.json();

    if (!response.ok) {
      throw new Error(responseData.members || "Failed to fetch");
    }

    const units = [];
    const actions = [];

    for (const res in responseData["units"]) {
      const unit = {
        id: responseData["units"][res]["id"],
        unit: responseData["units"][res]["unit"],
      };
      units.push(unit);
    }

    for (const res in responseData["actions"]) {
      const action = {
        id: responseData["actions"][res]["id"],
        status: responseData["actions"][res]["status"],
      };
      actions.push(action);
    }

    const reports = [];
    for (const res in responseData["reports"]) {
      const report = {
        id: responseData["reports"][res]["id"],
        content: responseData["reports"][res]["content"],
        desc: responseData["reports"][res]["desc"],
        actionStatus: responseData["reports"][res]["actionStatus"],
        email: responseData["reports"][res]["email"],
        firstRead: responseData["reports"][res]["firstRead"],
        firstReadDate: responseData["reports"][res]["firstReadDate"],
        confirm: responseData["reports"][res]["confirm"],
        tel: responseData["reports"][res]["tel"],
        userAgent: responseData["reports"][res]["userAgent"],
        unit: responseData["reports"][res]["unit"],
        readStatus: responseData["reports"][res]["readStatus"],
        addDate: responseData["reports"][res]["addDate"],
        comments: responseData["reports"][res]["comments"],
      };
      reports.push(report);
    }

    const newReports = [];

    for (const rep in reports) {
      if (
        reports[rep]["actionStatus"] ===
        actions.find((action) => action["status"] === "nowe")["id"]
      ) {
        newReports.push(reports[rep]);
      }
    }

    context.commit("loadReports", reports);
    context.commit("loadNewReports", newReports);
    context.commit("setActions", actions);
    context.commit("setUnits", units);
  },

  async setFirstReader(context, payload) {
    const firstReader = {
      id: payload.id,
      user: payload.user,
    };

    const response = await fetch("http://form/bok/firstReader", {
      method: "POST",
      body: JSON.stringify(firstReader),
    });

    if (!response.ok) {
      throw new Error("Błąd podczas wysyłania danych.");
    }

    const responseData = await response.json();

    context.commit("setFirstReader", {
      id: payload.id,
      firstReader: payload.user,
      firstDate: responseData["date"],
    });
  },

  async setStatus(context, payload) {
    const status = {
      reportId: payload.reportId,
      statusId: payload.statusId,
    };

    const response = await fetch("http://form/bok/setStatus", {
      method: "POST",
      body: JSON.stringify(status),
    });

    if (!response.ok) {
      throw new Error("Bład podczas wysyłania danych.");
    }

    context.commit("setStatus", {
      reportId: payload.reportId,
      statusId: payload.statusId,
    });
  },

  async addComment(context, payload) {
    const comment = {
      user: payload.user,
      comment: payload.comment,
      reportId: payload.reportId,
    };

    const response = await fetch("http://form/bok/addComment", {
      method: "POST",
      body: JSON.stringify(comment),
    });

    if (!response.ok) {
      throw new Error("Błąd wysyłania danych");
    }

    context.commit("addComment", {
      user: payload.user,
      comment: payload.comment,
      id: payload.reportId,
    });
  },
};
