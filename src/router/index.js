import { createRouter, createWebHashHistory } from "vue-router";
import HomeView from "@/views/HomeView.vue";
import BokView from "@/views/ReportsList.vue";
import NotFound from "@/views/NotFound.vue";
import SendReport from "@/views/SendReport.vue";
import ReportDetail from "@/views/ReportDetail.vue";

const routes = [
  {
    path: "/",
    redirect: "/bok/all",
    name: "home",
    component: HomeView,
  },
  {
    path: "/form",
    name: "form",
    component: SendReport,
  },
  {
    path: "/bok/all",
    name: "bok",
    component: BokView,
  },
  {
    path: "/bok/all/:id",
    name: "bok_id",
    component: ReportDetail,
    props: true,
  },
  {
    path: "/:notFound(.*)",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
